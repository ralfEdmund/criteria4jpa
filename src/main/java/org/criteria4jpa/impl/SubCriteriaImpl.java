package org.criteria4jpa.impl;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.criteria4jpa.Criteria;
import org.criteria4jpa.criterion.Criterion;
import org.criteria4jpa.order.Order;
import org.criteria4jpa.projection.Projection;

public class SubCriteriaImpl implements Criteria {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 9156219777154279835L;

	private final CriteriaImpl root;
	private final Criteria parent;
	private final String path;
	private final String alias;
	private final JoinType joinType;

	/**
	 * Creates a new {@link SubCriteriaImpl}
	 * 
	 * @param root
	 *            Reference to the root criteria
	 * @param parent
	 *            Reference to parent criteria
	 * @param path
	 *            relative path to the references entity or collection
	 * @param alias
	 *            Custom alias for this subcriteria (may be <code>null</code>)
	 * @param joinType
	 *            The type of join to perform
	 */
	public SubCriteriaImpl(CriteriaImpl root, Criteria parent, String path,
			String alias, JoinType joinType) {
		this.root = root;
		this.parent = parent;
		this.path = path;
		this.alias = alias;
		this.joinType = joinType;
	}

	/*
	 * implementation methods delegate to parent
	 */

	public Criteria add(Criterion criterion) {
		root.add(this, criterion);
		return this;
	}

	public Criteria addOrder(Order order) {
		root.addOrder(this, order);
		return this;
	}

	@SuppressWarnings("rawtypes")
	public List getResultList() {
		return root.getResultList();
	}

	public Criteria setFirstResult(int firstResult) {
		root.setFirstResult(firstResult);
		return this;
	}

	public Criteria setMaxResults(int maxResults) {
		root.setMaxResults(maxResults);
		return this;
	}

	public Object getSingleResult() {
		return root.getSingleResult();
	}

	public Object getSingleResultOrNull() {
		return root.getSingleResultOrNull();
	}

	public String getAlias() {
		return alias;
	}

	public Criteria createCriteria(String associationPath) {
		return root.createCriteria(this, associationPath, null,
				JoinType.INNER_JOIN);
	}

	public Criteria createCriteria(String associationPath, JoinType joinType) {
		return root.createCriteria(this, associationPath, null, joinType);
	}

	public Criteria createCriteria(String associationPath, String alias) {
		return root.createCriteria(this, associationPath, alias,
				JoinType.INNER_JOIN);
	}

	public Criteria createCriteria(String associationPath, String alias,
			JoinType joinType) {
		return root.createCriteria(this, associationPath, alias, joinType);
	}

	public Criteria setProjection(Projection projection) {
		root.setProjection(this, projection);
		return this;
	}

	public Projection getProjection() {
		return root.getProjection();
	}

	public Query buildQuery() {
		return root.buildQuery();
	}

	public List<MetaEntry<Criterion>> getCriterionList() {
		return root.getCriterionList();
	}

	public List<MetaEntry<Order>> getOrderList() {
		return root.getOrderList();
	}

	public List<SubCriteriaImpl> getSubcriteriaList() {
		return root.getSubcriteriaList();
	}

	public String getPath() {
		return path;
	}

	public Criteria getParent() {
		return parent;
	}

	public CriteriaImpl getRoot() {
		return root;
	}

	public JoinType getJoinType() {
		return joinType;
	}

	@Override
	public void addHint(String key, Object value) {
		root.addHint(key, value);
	}

	@Override
	public TypedQuery<?> buildTypedQuery() {
		return root.buildTypedQuery();
	}

	@SuppressWarnings("rawtypes")
	public Class getRowType() {
		return root.getRowType();
	}
	
	
}
