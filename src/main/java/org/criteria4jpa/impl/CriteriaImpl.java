package org.criteria4jpa.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.criteria4jpa.Criteria;
import org.criteria4jpa.criterion.Criterion;
import org.criteria4jpa.order.Order;
import org.criteria4jpa.projection.Projection;

/**
 * 
 * @author Michael.Helbig
 * 
 */
@SuppressWarnings("rawtypes")
public class CriteriaImpl<T> implements Criteria<T> {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 7481373021740563314L;

	// basic stuff
	private final EntityManager entityManager;
	private final String entityName;
	private final String alias;

	// properties
	private final List<MetaEntry<Criterion>> criterionList = new ArrayList<MetaEntry<Criterion>>();
	private final List<MetaEntry<Order>> orderList = new ArrayList<MetaEntry<Order>>();
	private final List<SubCriteriaImpl> subcriteriaList = new ArrayList<SubCriteriaImpl>();
	private MetaEntry<Projection> projectionEntry;
	private Integer maxResults;
	private Integer firstResult;

	private Class clazz;
	private Map<String, Object> hints;

	public CriteriaImpl(EntityManager entityManager, Class clazz) {
		this(entityManager, clazz, null);
	}

	public CriteriaImpl(EntityManager entityManager, Class clazz, String alias) {
		this.entityManager = entityManager;
		this.entityName = clazz.getSimpleName();
		this.alias = alias;
		this.clazz = clazz;

		this.hints = new HashMap<String, Object>();
	}

	/**
	 * 
	 * @param key
	 * @param value
	 */
	public void addHint(String key, Object value) {
		this.hints.put(key, value);
	}

	/*
	 * internal
	 */

	public void addOrder(Criteria criteria, Order order) {
		this.orderList.add(new MetaEntry<Order>(criteria, order));
	}

	public Criteria add(Criteria criteria, Criterion criterion) {
		this.criterionList.add(new MetaEntry<Criterion>(criteria, criterion));
		return this;
	}

	protected Criteria createCriteria(Criteria parent, String associationPath,
			String alias, JoinType joinType) {
		SubCriteriaImpl subcriteria = new SubCriteriaImpl(this, parent,
				associationPath, alias, joinType);
		subcriteriaList.add(subcriteria);
		return subcriteria;
	}

	protected void setProjection(Criteria criteria, Projection projection) {
		if (criteria != null)
			projectionEntry = new MetaEntry<Projection>(criteria, projection);
		else
			projectionEntry = null;
	}

	/*
	 * interface impl
	 */

	public Criteria<T> add(Criterion criterion) {
		add(this, criterion);
		return this;
	}

	public Criteria<T> addOrder(Order order) {
		addOrder(this, order);
		return this;
	}

	public Criteria<?> createCriteria(String associationPath) {
		return createCriteria(this, associationPath, null, JoinType.INNER_JOIN);
	}

	public Criteria<?> createCriteria(String associationPath, JoinType joinType) {
		return createCriteria(this, associationPath, null, joinType);
	}

	public Criteria<?> createCriteria(String associationPath, String alias) {
		return createCriteria(this, associationPath, alias, JoinType.INNER_JOIN);
	}

	public Criteria<?> createCriteria(String associationPath, String alias,
			JoinType joinType) {
		return createCriteria(this, associationPath, alias, joinType);
	}

	public Criteria<T> setProjection(Projection projection) {
		setProjection(this, projection);
		return this;
	}

	public Criteria<T> setFirstResult(int firstResult) {
		this.firstResult = Integer.valueOf(firstResult);
		return this;
	}

	public Criteria<T> setMaxResults(int maxResults) {
		this.maxResults = Integer.valueOf(maxResults);
		return this;
	}

	/*
	 * execute query
	 */

	@SuppressWarnings("unchecked")
	public List<T> getResultList() {
		return buildQuery().getResultList();
	}

	@SuppressWarnings("unchecked")
	public T getSingleResult() {
		return (T) buildQuery().getSingleResult();
	}

	/**
	 * Liefert das Single Object Or Null
	 * 
	 */
	public T getSingleResultOrNull() {
		try {
			return getSingleResult();
		} catch (NonUniqueResultException e) {
			return null;
		} catch (NoResultException e) {
			return null;
		}
	}

	/*
	 * internal stuff
	 */

	public Query buildQuery() {
		CriteriaQueryBuilder queryBuilder = new CriteriaQueryBuilder(
				entityManager, this);
		Query query = queryBuilder.createQuery();
		if (!hints.isEmpty()) {
			for (String key : hints.keySet()) {
				Object value = hints.get(key);
				query.setHint(key, value);
			}
		}
		return query;
	}

	/**
	 * 
	 * @return TypedQuery<?>
	 */
	public TypedQuery<T> buildTypedQuery() {
		CriteriaQueryBuilder queryBuilder = new CriteriaQueryBuilder(
				entityManager, this);
		@SuppressWarnings("unchecked")
		TypedQuery<T> query = (TypedQuery<T>) queryBuilder
				.createTypedQuery(clazz);
		if (!hints.isEmpty()) {
			for (String key : hints.keySet()) {
				Object value = hints.get(key);
				query.setHint(key, value);
			}
		}
		return query;
	}

	/*
	 * internal stuff
	 */
	public Integer getMaxResults() {
		return maxResults;
	}

	public Integer getFirstResult() {
		return firstResult;
	}

	public String getEntityName() {
		return entityName;
	}

	public String getAlias() {
		return alias;
	}

	public List<MetaEntry<Criterion>> getCriterionList() {
		return criterionList;
	}

	public List<MetaEntry<Order>> getOrderList() {
		return orderList;
	}

	public List<SubCriteriaImpl> getSubcriteriaList() {
		return subcriteriaList;
	}

	public MetaEntry<Projection> getProjectionEntry() {
		return projectionEntry;
	}

	public Projection getProjection() {
		if (projectionEntry != null)
			return projectionEntry.getEntry();
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<T> getRowType() {
		return clazz;
	}
	
	
}
