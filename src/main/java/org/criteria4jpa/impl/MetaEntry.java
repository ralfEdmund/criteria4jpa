package org.criteria4jpa.impl;

import java.io.Serializable;

import org.criteria4jpa.Criteria;

public class MetaEntry<E> implements  Serializable {

  /**
	 * 
	 */
	private static final long serialVersionUID = -5805272961494710664L;

	private final Criteria criteria;
	private final E entry;

	public MetaEntry(Criteria criteria, E entry) {
		this.criteria = criteria;
		this.entry = entry;
	}

	public Criteria getCriteria() {
		return criteria;
	}

	public E getEntry() {
		return entry;
	}
  
}
