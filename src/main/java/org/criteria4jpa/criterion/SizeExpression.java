/**
 * 
 */
package org.criteria4jpa.criterion;

import org.criteria4jpa.Criteria;
import org.criteria4jpa.impl.CriteriaQueryBuilder;

/**
 * @author Michael.Helbig
 * @version 0.1.3
 *
 */
public class SizeExpression implements Criterion {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1847728277449923774L;
	
	private String relativePath;
	private Integer size;
	private String op;
	
	/**
	 * 
	 */
	public SizeExpression(String relativePath, Integer size, String op) {
		this.relativePath = relativePath;
		this.size = size;
		this.op = op;
	}

	
	/**
	 * 
	 */
	public SizeExpression(String relativePath, Integer size) {
		this.relativePath = relativePath;
		this.size = size;
		this.op = "=";
	}
	
	/* (non-Javadoc)
	 * @see org.criteria4jpa.criterion.Criterion#toQueryString(org.criteria4jpa.Criteria, org.criteria4jpa.impl.CriteriaQueryBuilder)
	 */
	@Override
	public String toQueryString(Criteria criteria, CriteriaQueryBuilder queryBuilder) {

	    // query builder
	    StringBuilder builder = new StringBuilder();

	    builder.append( " SIZE(");
	    // build the basic query
	    builder.append( queryBuilder.getAbsolutePath(criteria, relativePath) );
	    builder.append( ") ").append(op).append(" ");
        builder.append( queryBuilder.createPositionalParameter() );

	    // return result
	    return builder.toString();
	}

	/* (non-Javadoc)
	 * @see org.criteria4jpa.criterion.Criterion#getParameterValues()
	 */
	@Override
	public Object[] getParameterValues() {
	    return new Object[] { size };
	}

}
